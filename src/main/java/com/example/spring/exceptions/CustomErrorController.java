package com.example.spring.exceptions;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping(path = "/error")
@Controller
public class CustomErrorController implements ErrorController {

    static String messageStatic;

    static final String ERROR_HEADER = "<h1>Error Occured</h1>";

    static final String STANDARD_ERROR_MESSAGE = "Check your Domain";

    public static void setMessage(String message) {
        messageStatic = message;
    }

    @RequestMapping(path = "")
    @ResponseBody
    ResponseEntity errorHere() {
        HttpStatus httpStatus;
        //Website Header
        String returnMessage = ERROR_HEADER;
        //Creating a Standard Response
        String errorMessage = STANDARD_ERROR_MESSAGE;
        if (messageStatic != null) {
            errorMessage = messageStatic;
            httpStatus = HttpStatus.NOT_FOUND;
        } else {
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        //adding errormessage to Header
        returnMessage += errorMessage;
        ResponseEntity response = new ResponseEntity<>(returnMessage, httpStatus);
        messageStatic = null;
        return response;
    }
}
package com.example.spring.entityTemplates.extended;

import com.example.spring.entityTemplates.base.BaseEntity;
import com.example.spring.tables.TypeOfState;
import jakarta.persistence.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.util.Date;

// This is a template class for every Entity which will only have to be extended by whatever needed
@MappedSuperclass
//A Superclass with superclass / gets extended for all Entities
//with name, that we have a base for Entities with and without name
public class ExtendedBaseEntity extends BaseEntity {

    @Column
    private String name;

    @CreatedDate
    @Column(name = "created_at", nullable = false, updatable = false,
            columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date createdAt;

    @LastModifiedDate
    @Column(name = "updated_at", nullable = false,
            columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    private Date updatedAt;

    @Column(name = "deleted_at")
    private Date deletedAt;

    @PrePersist
    private void created() {
        createdAt = new Date();
        updatedAt = new Date();
        state = TypeOfState.published;
    }

    //this sets all Dates and the state before the object is generated to avoid null values being added and causing errors
    @PreUpdate
    private void modified() {
        //This will set the timestamp of updated at whenever an object is modified
        updatedAt = new Date();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public String toString() {
        return "TableTemplate{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", state=" + getState() +
                ", createdAt=" + getCreatedAt() +
                ", updatedAt=" + getUpdatedAt() +
                ", deletedAt=" + getDeletedAt() +
                '}';
    }
}

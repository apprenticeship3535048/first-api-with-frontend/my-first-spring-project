package com.example.spring.entityTemplates.extended;

import com.example.spring.entityTemplates.base.BaseController;
import com.example.spring.entityTemplates.DTO.TableDTO;
import com.example.spring.utils.SearchResult;
import com.example.spring.utils.SearchResultDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.function.Function;

public class ExtendedBaseController<Mapped extends TableDTO> extends BaseController {

    protected final ExtendedBaseService extendedService;


    protected ExtendedBaseController(ExtendedBaseService service, Function function) {
        super(service, function);
        this.extendedService = service;
    }

    @GetMapping( "/search")
    public SearchResult getByName(@RequestParam String name, @RequestParam int limit, @RequestParam int page){
        return extendedService.searchByName(name,limit,page);
    }
    @GetMapping("/mapped-search")
    public SearchResultDTO getByNameMapped(@RequestParam String name, @RequestParam int limit, @RequestParam int page){
        return extendedService.searchByNameMapped(name, limit, page);
    }
}

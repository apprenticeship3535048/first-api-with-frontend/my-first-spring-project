package com.example.spring.entityTemplates.extended;

import com.example.spring.entityTemplates.base.BaseRepo;
import com.example.spring.entityTemplates.DTO.TableDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import java.util.List;

@NoRepositoryBean
public interface ExtendedBaseRepo<Entity extends ExtendedBaseEntity, ID extends Integer, Map extends TableDTO>
        extends BaseRepo<Entity, ID, Map> {

    List<Entity> findEntitiesByNameContaining(@Param(value = "name") String name, Pageable pageable);

    int countEntitiesByNameContaining(String name);

}

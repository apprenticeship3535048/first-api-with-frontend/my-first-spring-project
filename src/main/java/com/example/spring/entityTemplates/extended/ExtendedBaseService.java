package com.example.spring.entityTemplates.extended;

import com.example.spring.entityTemplates.base.BaseEntity;
import com.example.spring.entityTemplates.base.BaseRepo;
import com.example.spring.entityTemplates.base.BaseService;
import com.example.spring.entityTemplates.DTO.NameIdDTO;
import com.example.spring.entityTemplates.DTO.TableDTO;
import com.example.spring.utils.OffsetLimitPageable;
import com.example.spring.utils.SearchResult;
import com.example.spring.utils.SearchResultDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


public abstract class ExtendedBaseService<Entity extends ExtendedBaseEntity,
        ExtendedRepository extends ExtendedBaseRepo<Entity, Integer, NameIdDTO>, Mapper extends Function<BaseEntity, TableDTO>>
            extends BaseService<Entity, BaseRepo<BaseEntity, Integer, TableDTO>, Function<BaseEntity, TableDTO>> {


    protected final ExtendedRepository extendedRepository;

    protected ExtendedBaseService(ExtendedRepository repo, Mapper baseEntityTableDTOFunction) {
        super((BaseRepo)repo, baseEntityTableDTOFunction);
        this.extendedRepository = (ExtendedRepository) repo;
    }

    protected SearchResult searchByName(String name, int limit, int page) {
        int amount = extendedRepository.countEntitiesByNameContaining(name);
        Pageable pageable = new OffsetLimitPageable(limit, page);
        List<Entity> entityList = extendedRepository.findEntitiesByNameContaining(name, pageable);
        SearchResult results = new SearchResult<>(amount, entityList);

        return results;
    }

    protected SearchResultDTO searchByNameMapped(String name, int limit, int page) {
        int amount = extendedRepository.countEntitiesByNameContaining(name);
        Pageable pageable = new OffsetLimitPageable(limit, page);
        List<Entity> entityList = extendedRepository.findEntitiesByNameContaining(name, pageable);
        entityList.stream().map(mapper).collect(Collectors.toList());
        SearchResultDTO results = new SearchResultDTO<>(amount, entityList.stream().map(mapper).collect(Collectors.toList()));

        return results;
    }

    /* public SearchResult searchByName (String name, int limit, int page){

        Pageable pageable = new OffsetLimitPageable(limit, page);
        List<Song> songs = songsRepository.findSongsByNameContaining(name,pageable);

        return results;
    }*/
}

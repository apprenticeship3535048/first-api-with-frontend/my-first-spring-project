package com.example.spring.entityTemplates.DTO;

import com.example.spring.entityTemplates.extended.ExtendedBaseEntity;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class NameIdDTOMapper implements Function<ExtendedBaseEntity, NameIdDTO> {

    @Override
    public NameIdDTO apply(ExtendedBaseEntity extendedBaseEntity) {
        return new NameIdDTO(
                extendedBaseEntity.getId(),
                extendedBaseEntity.getName()
        );
    }
}

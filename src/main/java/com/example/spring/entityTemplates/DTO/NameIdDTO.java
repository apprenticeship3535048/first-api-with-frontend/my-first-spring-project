package com.example.spring.entityTemplates.DTO;

public record NameIdDTO(
        int id,
        String name
) implements TableDTO {
}

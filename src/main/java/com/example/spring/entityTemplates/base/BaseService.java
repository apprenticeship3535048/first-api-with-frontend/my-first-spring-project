package com.example.spring.entityTemplates.base;

import com.example.spring.exceptions.IdNotFoundException;
import com.example.spring.entityTemplates.DTO.TableDTO;
import com.example.spring.tables.bands.classes.Band;
import com.example.spring.tables.labels.classes.Label;
import com.example.spring.tables.members.classes.Member;
import com.example.spring.tables.releases.classes.Release;
import com.example.spring.tables.roles.classes.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public abstract class BaseService<Entity extends BaseEntity, Repository extends BaseRepo<BaseEntity, Integer, TableDTO>
        , Mapper extends Function<BaseEntity, TableDTO>> {
    //This class is the template for all Service classes of each Entity
    protected final Repository repository;
    protected final Mapper mapper;

    protected BaseService(Repository repository, Mapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    } //That Depending on the service that uses its information the fitting mapper und repositories are used
    // mapper and repositories get used over a super constructor that links the Service with the ServiceTemplate


    public TableDTO getMappedByID(int id) {
        return repository.findById(id)
                .map(mapper)
                .orElseThrow(() -> {
                    throw new IdNotFoundException(getTableClass(), id);
                });
    } //get entry by id as mapped dto

    public BaseEntity getById(int id) {
        return repository.findById(id).orElseThrow(() -> {
            throw new IdNotFoundException(getTableClass(), id);
        });
    } // get entry by id

    public Page<BaseEntity> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    } // get all entries

    public String getTableClass() {
        //Get Classname: com.example.spring.tables.bands.classes.BandService
        String tableName = String.valueOf(getClass());
        //Create Substring from last dot upwards to delete the Path: BandService
        tableName = tableName.substring(tableName.lastIndexOf(".") + 1).toString();
        //Subtracting Service: Band
        tableName = tableName.replace("Service", "");
        return tableName;
    } //used for exception handling


    @Transactional
    public void deleteEntry(int id) {
        Entity entity = (Entity) getById(id);
        Integer parent_id = entity.getId();
        if (entity instanceof Release) {
            repository.deleteSongsByReleaseId(Collections.singletonList(parent_id));
        } else if (entity instanceof Band) {
            List<Integer> releaseIdList = new ArrayList<>();
            for (Release release : ((Band) entity).getReleases()) {
                releaseIdList.add(release.getId());
            }
            repository.deleteBandsMembersRolesByBandId(Collections.singletonList(parent_id));
            repository.deleteReleasesByIds(releaseIdList);
            repository.deleteSongsByReleaseId(releaseIdList);

        } else if (entity instanceof Label) {
            List<Band> bands = ((Label) entity).getBands();
            List<Integer> releaseIdList = new ArrayList<>();
            List<Integer> bandIDList = new ArrayList<>();

            for (Band band : bands) {
                bandIDList.add(band.getId());
                for (int j = 0; j < band.getReleases().size(); j++) {

                    releaseIdList.add(band.getReleases().get(j).getId());
                }
            }

            repository.deleteSongsByReleaseId(releaseIdList);
            repository.deleteReleasesByIds(releaseIdList);
            repository.deleteBandsByLabelId(parent_id);
            repository.deleteBandsMembersRolesByBandId(bandIDList);

        } else if (entity instanceof Member) repository.deleteBandsMembersRolesByMemberId(entity.getId());
        else if (entity instanceof Role) repository.deleteBandsMembersRolesByRoleId(entity.getId());

        repository.delete(entity);
    } // delete an entry
}

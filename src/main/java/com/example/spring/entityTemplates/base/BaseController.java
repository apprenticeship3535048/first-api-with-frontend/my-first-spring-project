package com.example.spring.entityTemplates.base;

import com.example.spring.entityTemplates.DTO.TableDTO;
import com.example.spring.tables.bands.classes.Band;
import com.example.spring.tables.labels.classes.Label;
import com.example.spring.tables.releases.classes.Release;
import com.example.spring.utils.OffsetLimitPageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.function.Function;

import static com.example.spring.tables.bands.classes.DTO.BandDTO.DTOFactory;
import static com.example.spring.tables.labels.classes.DTO.LabelDTO.DTOFactory;
import static com.example.spring.tables.releases.classes.DTO.ReleaseDTO.DTOFactory;

//This class is the template for all Controller classes of each Entity
@CrossOrigin(origins = "http://localhost:3000")
public abstract class BaseController<Mapper extends Function<BaseEntity, TableDTO>, DTO extends TableDTO, Entity extends BaseEntity> {

    protected final BaseService service;
    protected final Mapper mapper;

    protected BaseController(BaseService service, Mapper mapper) {
        //That Depending on the service that uses its information the fitting mapper und repositories are used
        //mapper and repositories get used over a super constructor that links the Service with the ServiceTemplate
        this.service = service;
        this.mapper = mapper;
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("")
    public Page<BaseEntity> index(@RequestParam int limit, @RequestParam int page,
                                  @RequestParam(required = false) Integer childlimit,
                                  @RequestParam(required = false) Integer childpage) {

        Pageable parentPageable = new OffsetLimitPageable(limit, page);


        /*Pageable bandPage = new OffsetLimitPageable(childlimit, childpage);

        Page<Label> labels = service.getAll(parentPageable);*/

        //Page<LabelDTO> labelDTOS = labels.map(label -> LabelDTO.labelDTOFactory(label,bandPage));

        return service.getAll(parentPageable);
    } //get all Entries

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/mapped")
    public Page<TableDTO> indexMapped(@RequestParam int limit, @RequestParam int page,
                                      @RequestParam(required = false) Integer select, //TODO: add multiple selects (ids) to iterate
                                      @RequestParam(required = false) Integer childlimit,
                                      @RequestParam(required = false, name = "child_page") Integer childpage) {

        Pageable parentPageable = new OffsetLimitPageable(limit, page);
        Page<Entity> entities = service.getAll(parentPageable);
        Pageable childPageable;

        if (childpage == null) {
            childPageable = null;
        } else {
            childPageable = new OffsetLimitPageable(childlimit, childpage);
        }
        final Pageable standard = new OffsetLimitPageable(5,0);

        Entity entity;
        Page<DTO> DTO;
        if (entities.getNumberOfElements() == 0) entity = null;
        else entity = entities.getContent().get(0);

        if (entity instanceof Label) {

            if (select == 0) DTO = (Page<DTO>) entities.map(label -> DTOFactory((Label) label, childPageable));
            else DTO = (Page<DTO>) entities.map(label -> {
                if (label.getId() == select) {
                    return DTOFactory((Label) label, childPageable);
                } else return DTOFactory((Label) label, standard);
            });
            return (Page<TableDTO>) DTO;
        }

        else if (entity instanceof Band){

            if (select == 0) DTO = (Page<DTO>) entities.map(band -> DTOFactory((Band) band, childPageable));
            else DTO = (Page<DTO>) entities.map(band -> {
                if (band.getId() == select) {
                    return DTOFactory((Band) band, childPageable);
                } else return DTOFactory((Band) band, standard);
            });
            return (Page<TableDTO>) DTO;
        }

        else if (entity instanceof Release){

            if (select == 0) DTO = (Page<DTO>) entities.map(release -> DTOFactory((Release) release, childPageable));
            else DTO = (Page<DTO>) entities.map(release -> {
                if (release.getId() == select) {
                    return DTOFactory((Release) release, childPageable);
                } else return DTOFactory((Release) release, standard);
            });
            return (Page<TableDTO>) DTO;
        }

        else return entities.map(mapper);

    } //get all Entries mapped by DTO

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}")
    public BaseEntity getByID(@PathVariable String id) {
        int table_id = Integer.parseInt(id);
        return service.getById(table_id);
    } //get specific entry by ID

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{id}/mapped")
    public TableDTO getByIdMapped(@PathVariable int id) {
        return service.getMappedByID(id);
    } //get specific entry by ID in mapped DTO format

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{id}")
    public void deleteEntry(@PathVariable int id) {
        service.deleteEntry(id);
    }
}

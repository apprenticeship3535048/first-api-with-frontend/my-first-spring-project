package com.example.spring.entityTemplates.base;

import com.example.spring.tables.TypeOfState;
import jakarta.persistence.*;

// This is a template class for every Entity which will only have to be extended by whatever needed
@MappedSuperclass
public class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private int id;

    @Column(name = "state", nullable = false,
            columnDefinition = "varchar(32) default 'published'")
    @Enumerated(EnumType.STRING)
    public TypeOfState state;


    public BaseEntity() {
    }

    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    public TypeOfState getState() {
        return state;
    }

    public void setState(TypeOfState state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "TableTemplate{" +
                "id=" + id +
                '}';
    }
}

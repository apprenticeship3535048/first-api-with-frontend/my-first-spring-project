package com.example.spring.entityTemplates.base;

import com.example.spring.entityTemplates.DTO.TableDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import java.util.List;


@NoRepositoryBean
public interface BaseRepo<Entity extends BaseEntity, ID extends Integer,
        Map extends TableDTO> extends JpaRepository<Entity, ID> {

    @Modifying
    @Query(value = "update songs set deleted_at = current_date, state = 'deleted' where release_id IN :release_id", nativeQuery = true)
    void deleteSongsByReleaseId(@Param("release_id") List<Integer> release_id);
    @Modifying
    @Query(value = "update releases set deleted_at = current_date, state = 'deleted' where id IN :band", nativeQuery = true)
    void deleteReleasesByIds(@Param("band") List<Integer> band);
    @Modifying
    @Query(value = "update bands set deleted_at = current_date, state = 'deleted' where label_id = :label_id", nativeQuery = true)
    void deleteBandsByLabelId(@Param("label_id") Integer label_id);
    @Modifying
    @Query(value = "update bands_members_roles set state = 'deleted' where band_id IN (:band_id)", nativeQuery = true)
    void deleteBandsMembersRolesByBandId(@Param("band_id") List<Integer> band_id);
    @Modifying
    @Query(value = "update bands_members_roles set state = 'deleted' where member_id = :member_id", nativeQuery = true)
    void deleteBandsMembersRolesByMemberId(@Param("member_id") Integer member_id);
    @Modifying
    @Query(value = "update bands_members_roles set state = 'deleted' where role_id = :role_id", nativeQuery = true)
    void deleteBandsMembersRolesByRoleId(@Param("role_id") Integer role_id);

}

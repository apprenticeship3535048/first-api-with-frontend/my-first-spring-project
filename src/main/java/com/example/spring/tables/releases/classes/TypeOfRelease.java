package com.example.spring.tables.releases.classes;

public enum TypeOfRelease {
    Extended_Play,
    Album,
    Single
}

package com.example.spring.tables.releases;

import com.example.spring.entityTemplates.extended.ExtendedBaseRepo;
import com.example.spring.tables.releases.classes.DTO.ReleaseDTO;
import com.example.spring.tables.releases.classes.Release;
import org.springframework.stereotype.Repository;

@Repository
public interface ReleaseRepository extends ExtendedBaseRepo<Release, Integer, ReleaseDTO> {

}

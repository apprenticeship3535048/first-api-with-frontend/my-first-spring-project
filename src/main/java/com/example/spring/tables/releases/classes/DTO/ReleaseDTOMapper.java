package com.example.spring.tables.releases.classes.DTO;

import com.example.spring.tables.releases.classes.Release;
import com.example.spring.tables.songs.classes.DTO.SongDTOMapper;
import com.example.spring.entityTemplates.DTO.NameIdDTOMapper;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ReleaseDTOMapper implements Function<Release, ReleaseDTO> {

    @Override
    public ReleaseDTO apply(Release release) {
        return new ReleaseDTO(
                release.getId(),
                release.getName(),
                release.getNumOfSales(),
                release.getReleaseYear(),
                release.getGenres().stream().map(new NameIdDTOMapper()).collect(Collectors.toList()),
                new PageImpl<>(release.getSongs()
                        .stream()
                        .map(new SongDTOMapper())
                        .collect(Collectors.toList()))
        );
    }
}

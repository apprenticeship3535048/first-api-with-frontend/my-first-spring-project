package com.example.spring.tables.releases.classes;

import com.example.spring.entityTemplates.extended.ExtendedBaseController;
import com.example.spring.tables.releases.ReleaseRepository;
import com.example.spring.tables.releases.classes.DTO.ReleaseDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(path = "api/releases")
public class ReleaseController extends ExtendedBaseController {
    @Autowired
    ReleaseRepository releaseRepository;
    @Autowired
    ReleaseService releaseService;

    protected ReleaseController(ReleaseService releaseService, ReleaseDTOMapper releaseDTOMapper) {
        super(releaseService, releaseDTOMapper);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/band/{id}")
    public Release update(@PathVariable String id, @RequestBody Map<String, String> body) {
        int member_id = Integer.parseInt(id);
        Release release = releaseRepository.findById(member_id).orElse(null);
        release.setName(body.get("title"));
        return releaseRepository.save(release);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    public Release createNew(@RequestBody Release release) {

        return releaseService.createNew(release);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public Release updateEntry(@PathVariable int id, @RequestBody Release release) {

        return releaseService.update(id, release);
    }
}

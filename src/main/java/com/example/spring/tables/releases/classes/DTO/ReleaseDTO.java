package com.example.spring.tables.releases.classes.DTO;

import com.example.spring.entityTemplates.DTO.NameIdDTOMapper;
import com.example.spring.tables.bands.classes.Band;
import com.example.spring.tables.bands.classes.DTO.BandDTO;
import com.example.spring.tables.bandsMembersRoles.classes.DTO.MemberRolesDTOMapper;
import com.example.spring.tables.releases.classes.Release;
import com.example.spring.tables.songs.classes.DTO.SongDTO;
import com.example.spring.entityTemplates.DTO.NameIdDTO;
import com.example.spring.entityTemplates.DTO.TableDTO;
import com.example.spring.tables.songs.classes.DTO.SongDTOMapper;
import com.example.spring.utils.OffsetLimitPageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.stream.Collectors;

public record ReleaseDTO(
        int id,
        String name,
        Long numberOfSales,
        Integer release_year,
        List<NameIdDTO> genres,
        Page<SongDTO> songs
) implements TableDTO {

    public static ReleaseDTO DTOFactory(Release release, Pageable pageable) {
        int[] regulators = new int[3];
        regulators = OffsetLimitPageable.regulate(release,pageable,regulators);
        int size = regulators[0];
        int offset = regulators[1];
        int limit = regulators[2];


        return new ReleaseDTO(
                release.getId(),
                release.getName(),
                release.getNumOfSales(),
                release.getReleaseYear(),
                release.getGenres()
                        .stream()
                        .map(new NameIdDTOMapper())
                        .collect(Collectors.toList()),
                new PageImpl<>(release.getSongs().stream().skip(offset)
                        .map(new SongDTOMapper())
                        .collect(Collectors.toList())
                        .subList(0,limit), pageable, size)
        );
    }
}

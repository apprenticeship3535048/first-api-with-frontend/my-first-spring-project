package com.example.spring.tables.releases.classes;

import com.example.spring.tables.bands.classes.Band;
import com.example.spring.tables.genres.classes.Genre;
import com.example.spring.tables.labels.classes.Label;
import com.example.spring.tables.songs.classes.Song;
import com.example.spring.entityTemplates.extended.ExtendedBaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.*;

@Entity
@Table(name = "releases")
@SQLDelete(sql = "UPDATE releases SET state = 'deleted', deleted_at = current_date where id=?")
@Where(clause = "state = 'published'")
public class Release extends ExtendedBaseEntity {

    @Column(name = "type_of_release", nullable = false)
    @Enumerated(EnumType.STRING)
    private TypeOfRelease typeOfRelease;
    //for all options take a look in enum class "typeOfRelease"

    @Column(name = "num_of_sales")
    private Long numOfSales;

    @Column(name = "release_year")
    private Integer releaseYear;

    @ManyToMany
    @JoinTable(name = "releases_genres",
            joinColumns = @JoinColumn(name = "release_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id"))
    private List<Genre> genres = new ArrayList<>();
    //many-to-many reference to genres

    @OneToMany(mappedBy = "release")
    private List<Song> songs = new ArrayList<>();
    //one-to-many reference to songs, gives back an array of songs as json

    @JsonBackReference(value = "bands_releases")
    @ManyToMany
    @JoinTable(name = "bands_releases",
            joinColumns = @JoinColumn(name = "release_id"),
            inverseJoinColumns = @JoinColumn(name = "band_id"))
    private List<Band> bands = new ArrayList<>();
    //reference to bands with releases being the child

    @JsonBackReference(value = "release_label")
    @ManyToOne(fetch = FetchType.LAZY)
    private Label label;
    //reference to labels with releases being the child

    public Release() {
        super();
    }
    //La magique manifique


    //getters and setters
    public Long getNumOfSales() {
        return numOfSales;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer release_year) {
        this.releaseYear = release_year;
    }


    public void setNumOfSales(Long numOfSales) {
        this.numOfSales = numOfSales;
    }

    public TypeOfRelease getTypeOfRelease() {
        return typeOfRelease;
    }

    public void setTypeOfRelease(TypeOfRelease type_of_release) {
        this.typeOfRelease = type_of_release;
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    public List<Band> getBands() {
        return bands;
    }

    public void setBands(List<Band> bands) {
        this.bands = bands;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }
}

package com.example.spring.tables.releases.classes;


import com.example.spring.entityTemplates.extended.ExtendedBaseService;
import com.example.spring.tables.releases.ReleaseRepository;
import com.example.spring.tables.releases.classes.DTO.ReleaseDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReleaseService extends ExtendedBaseService {
    @Autowired
    ReleaseRepository releaseRepository;

    protected ReleaseService(ReleaseRepository releaseRepository, ReleaseDTOMapper releaseDTOMapper) {
        super(releaseRepository, releaseDTOMapper);
    }

    //add a new Release to the DB
    public Release createNew(Release release) {

        return releaseRepository.save(release);
    }

    //update a Release
    public Release update(int id, Release update) {
        Release release = (Release) getById(id);
        if (update.getName() != null) release.setName(update.getName());
        if (update.getLabel() != null) release.setLabel(update.getLabel());
        if (update.getReleaseYear() != null) release.setReleaseYear(update.getReleaseYear());
        if (update.getTypeOfRelease() != null) release.setTypeOfRelease(update.getTypeOfRelease());
        if (update.getNumOfSales() != null) release.setNumOfSales(update.getNumOfSales());
        if (update.getBands() != null) release.setBands(update.getBands());
        return releaseRepository.save(release);
    }


}

package com.example.spring.tables.roles;

import com.example.spring.entityTemplates.extended.ExtendedBaseRepo;
import com.example.spring.tables.roles.classes.DTO.RoleDTO;
import com.example.spring.tables.roles.classes.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends ExtendedBaseRepo<Role, Integer, RoleDTO> {
}

package com.example.spring.tables.roles.classes.DTO;

import com.example.spring.entityTemplates.DTO.TableDTO;

public record RoleDTO(
        int id,
        String name
) implements TableDTO {
}

package com.example.spring.tables.roles.classes;

import com.example.spring.entityTemplates.extended.ExtendedBaseService;
import com.example.spring.tables.roles.RoleRepository;
import com.example.spring.entityTemplates.DTO.NameIdDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService extends ExtendedBaseService {
    @Autowired
    RoleRepository roleRepository;

    protected RoleService(RoleRepository roleRepository, NameIdDTOMapper nameIdDTOMapper) {
        super(roleRepository, nameIdDTOMapper);
    }

    //add a new Role to the DB
    public Role createNew(Role role) {

        return roleRepository.save(role);
    }

    //update a Role
    public Role update(int id, Role update) {
        Role role = (Role) getById(id);
        if (update.getName() != null) role.setName(update.getName());

        return roleRepository.save(role);
    }
}

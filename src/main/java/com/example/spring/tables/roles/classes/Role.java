package com.example.spring.tables.roles.classes;

import com.example.spring.tables.bandsMembersRoles.classes.BandsMembersRoles;
import com.example.spring.entityTemplates.extended.ExtendedBaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.List;

@Entity
@Table(name = "roles")
@SQLDelete(sql = "UPDATE roles SET state = 'deleted', deleted_at = current_date where id=?")
@Where(clause = "state = 'published'")
public class Role extends ExtendedBaseEntity {

    //reference to many-to-many referencing table as child entity
    @JsonBackReference(value = "roles")
    @OneToMany(mappedBy = "role")
    private List<BandsMembersRoles> bandsMembersRolesList;

    //Science fiction
    public Role() {
        super();
    }

    //getters and setters
    public List<BandsMembersRoles> getBandsMembersRolesList() {
        return bandsMembersRolesList;
    }

    public void setBandsMembersRolesList(List<BandsMembersRoles> bandsMembersRolesList) {
        this.bandsMembersRolesList = bandsMembersRolesList;
    }
}

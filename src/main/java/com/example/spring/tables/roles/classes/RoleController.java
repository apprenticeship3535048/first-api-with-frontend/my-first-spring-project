package com.example.spring.tables.roles.classes;

import com.example.spring.entityTemplates.extended.ExtendedBaseController;
import com.example.spring.tables.roles.RoleRepository;
import com.example.spring.entityTemplates.extended.ExtendedBaseEntity;
import com.example.spring.tables.roles.classes.DTO.RoleDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(path = "api/roles")
public class RoleController extends ExtendedBaseController {
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    RoleService roleService;

    protected RoleController(RoleService roleService, RoleDTOMapper roleDTOMapper) {
        super(roleService, roleDTOMapper);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/band/{id}")
    public ExtendedBaseEntity update(@PathVariable String id, @RequestBody Map<String, String> body) {
        int role_id = Integer.parseInt(id);
        Role members = roleRepository.findById(role_id).orElse(null);
        members.setName(body.get("title"));
        return roleRepository.save(members);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    public Role createNew(@RequestBody Role role) {

        return roleService.createNew(role);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public Role updateEntry(@PathVariable int id, @RequestBody Role role) {

        return roleService.update(id, role);
    }

}

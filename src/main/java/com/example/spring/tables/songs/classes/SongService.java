package com.example.spring.tables.songs.classes;

import com.example.spring.entityTemplates.extended.ExtendedBaseService;
import com.example.spring.tables.songs.SongRepository;
import com.example.spring.tables.songs.classes.DTO.SongDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SongService extends ExtendedBaseService {


    SongRepository songsRepository;
    @Autowired
    protected SongService(SongRepository songsRepository, SongDTOMapper songDTOMapper) {
        super(songsRepository, songDTOMapper);
        this.songsRepository = songsRepository;
    }

    //add a new Song to the DB
    public Song createNew(Song song) {

        return songsRepository.save(song);
    }

    //update a Song
    public Song update(int id, Song update) {
        Song song = (Song) getById(id);
        if (update.getName() != null) song.setName(update.getName());
        if (update.getLength() != null) song.setLength(update.getLength());
        if (update.getPositionId() != null) song.setPositionId(update.getPositionId());
        if (update.getRelease() != null) song.setRelease(update.getRelease());
        return songsRepository.save(song);
    }
}

package com.example.spring.tables.songs.classes;

import com.example.spring.tables.releases.classes.Release;
import com.example.spring.entityTemplates.extended.ExtendedBaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "songs")
@SQLDelete(sql = "UPDATE songs SET state = 'deleted', deleted_at = current_date where id=?")
@Where(clause = "state = 'published'")
public class Song extends ExtendedBaseEntity {

    @Column(name = "length", nullable = false)
    private Integer length;

    @Column(name = "position_id")
    private Integer positionId;

    @JsonBackReference(value = "song_release")
    //song is referenced in release as a child entity
    @ManyToOne(fetch = FetchType.LAZY)
    private Release release;

    //the magical being
    public Song() {
        super();
    }

    //getters and setters
    public Release getRelease() {
        return release;
    }

    public void setRelease(Release release) {
        this.release = release;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }
}

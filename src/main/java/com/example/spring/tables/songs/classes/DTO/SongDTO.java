package com.example.spring.tables.songs.classes.DTO;

import com.example.spring.entityTemplates.DTO.TableDTO;

public record SongDTO(
        int id,
        String name,
        int length
) implements TableDTO {
}

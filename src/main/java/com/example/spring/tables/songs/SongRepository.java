package com.example.spring.tables.songs;

import com.example.spring.entityTemplates.extended.ExtendedBaseRepo;
import com.example.spring.tables.songs.classes.DTO.SongDTO;
import com.example.spring.tables.songs.classes.Song;

public interface SongRepository extends ExtendedBaseRepo<Song, Integer, SongDTO> {

    /*@Query(value = "select song from Song song where song.name like %:name%")
    List<Song> findSongsByNameContaining(String name, Pageable pageable);

    @Query(value = "select count(song) from Song song where song.name like %:name%")
    int getAmountOfResultsByNameContaining(String name);*/
}

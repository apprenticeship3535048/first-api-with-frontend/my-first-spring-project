package com.example.spring.tables.songs.classes.DTO;

import com.example.spring.tables.songs.classes.Song;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class SongDTOMapper implements Function<Song, SongDTO> {

    @Override
    public SongDTO apply(Song song) {
        return new SongDTO(
                song.getId(),
                song.getName(),
                song.getLength()
        );
    }
}

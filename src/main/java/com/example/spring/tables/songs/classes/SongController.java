package com.example.spring.tables.songs.classes;


import com.example.spring.entityTemplates.extended.ExtendedBaseController;
import com.example.spring.tables.songs.classes.DTO.SongDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/songs")
public class SongController extends ExtendedBaseController {

    SongService songService;

    @Autowired
    protected SongController(SongService songService, SongDTOMapper songDTOMapper) {
        super(songService, songDTOMapper);
        this.songService = songService;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    public Song createNew(@RequestBody Song song) {

        return songService.createNew(song);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public Song updateEntry(@PathVariable int id, @RequestBody Song song) {

        return songService.update(id, song);
    }

    /*@GetMapping("/search")
    public SearchResult getByName(@RequestParam String name, @RequestParam int limit, @RequestParam int page) {
        return songService.searchByName(name, limit, page);
    }*/

    /*@GetMapping("/mapped-search")
    public List<SongDTO> getByNameMapped(@RequestParam String name, @RequestParam int limit, @RequestParam int page) {
        return (List<SongDTO>) songService.searchByName(name, limit, page)
                .stream()
                .map(mapper)
                .collect(Collectors.toList());
    }*/
}

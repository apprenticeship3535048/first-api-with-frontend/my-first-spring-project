package com.example.spring.tables.genres.classes.DTO;

import com.example.spring.entityTemplates.DTO.TableDTO;

public record GenreDTO(
        int id,
        String name
) implements TableDTO {
}

package com.example.spring.tables.genres.classes;

import com.example.spring.entityTemplates.extended.ExtendedBaseService;
import com.example.spring.tables.genres.GenreRepository;
import com.example.spring.entityTemplates.DTO.NameIdDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenreService extends ExtendedBaseService {
    @Autowired
    GenreRepository genreRepository;

    protected GenreService(GenreRepository genreRepository, NameIdDTOMapper nameIdDTOMapper) {
        super(genreRepository, nameIdDTOMapper);
    }


    public Genre createNew(Genre genre) {
        return genreRepository.save(genre);
    }  //add a new Genre to the DB

    //update a Genre
    public Genre update(int id, Genre update) {
        Genre genre = (Genre) getById(id);
        if (update.getName() != null) genre.setName(update.getName());
        return genreRepository.save(genre);
    }
}

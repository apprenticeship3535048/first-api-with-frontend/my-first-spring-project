package com.example.spring.tables.genres;

import com.example.spring.entityTemplates.extended.ExtendedBaseRepo;
import com.example.spring.tables.genres.classes.DTO.GenreDTO;
import com.example.spring.tables.genres.classes.Genre;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository extends ExtendedBaseRepo<Genre, Integer, GenreDTO> {

    List<Genre> findGenresByNameContaining(String name, Pageable pageable);
}

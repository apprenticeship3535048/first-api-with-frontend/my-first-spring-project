package com.example.spring.tables.genres.classes.DTO;

import com.example.spring.tables.genres.classes.Genre;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class GenreDTOMapper implements Function<Genre, GenreDTO> {

    @Override
    public GenreDTO apply(Genre genre) {
        return new GenreDTO(
                genre.getId(),
                genre.getName()
        );
    }
}

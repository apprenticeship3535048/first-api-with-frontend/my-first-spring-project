package com.example.spring.tables.genres.classes;

import com.example.spring.tables.releases.classes.Release;
import com.example.spring.entityTemplates.extended.ExtendedBaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.*;

@Entity
@Table(name = "genres")
@SQLDelete(sql = "UPDATE genres SET state = 'deleted', deleted_at = current_date where id=?")
@Where(clause = "state = 'published'")
public class Genre extends ExtendedBaseEntity {

    //magical constructor thing
    public Genre() {
        super();
    }

    @JsonBackReference
    //genres is a child of releases
    @ManyToMany(mappedBy = "genres")
    private List<Release> releases = new ArrayList<>();

    public List<Release> getReleases() {
        return releases;
    }

    public void setReleases(List<Release> releases) {
        this.releases = releases;
    }
}

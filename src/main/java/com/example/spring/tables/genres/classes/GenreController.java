package com.example.spring.tables.genres.classes;

import com.example.spring.entityTemplates.extended.ExtendedBaseController;
import com.example.spring.tables.genres.GenreRepository;
import com.example.spring.tables.genres.classes.DTO.GenreDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/genres")
public class GenreController extends ExtendedBaseController {
    @Autowired
    GenreRepository genreRepository;
    @Autowired
    GenreService genreService;
    protected GenreController(GenreService genreService, GenreDTOMapper genreDTOMapper) {
        super(genreService, genreDTOMapper);

    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    public Genre createNew(@RequestBody Genre genre) {

        return genreService.createNew(genre);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public Genre updateEntry(@PathVariable int id, @RequestBody Genre genre) {

        return genreService.update(id, genre);
    }
}
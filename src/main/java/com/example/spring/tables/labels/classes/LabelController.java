package com.example.spring.tables.labels.classes;


import com.example.spring.entityTemplates.base.BaseEntity;
import com.example.spring.entityTemplates.extended.ExtendedBaseController;
import com.example.spring.tables.bands.classes.BandService;
import com.example.spring.tables.bands.classes.DTO.BandDTOMapper;
import com.example.spring.tables.labels.LabelRepository;
import com.example.spring.tables.labels.classes.DTO.LabelDTO;
import com.example.spring.tables.labels.classes.DTO.LabelDTOMapper;
import com.example.spring.utils.OffsetLimitPageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(path = "api/labels")
public class LabelController extends ExtendedBaseController {

    LabelService labelService;
    protected LabelController(LabelService labelService,
                              LabelDTOMapper labelDTOMapper) {
        super(labelService, labelDTOMapper);
        this.labelService = labelService;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    public Label createNew(@RequestBody Label label) {
        return labelService.createNew(label);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public Label updateEntry(@PathVariable int id, @RequestBody Label label) {

        return labelService.update(id, label);
    }
}

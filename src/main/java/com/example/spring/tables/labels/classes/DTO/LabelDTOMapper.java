package com.example.spring.tables.labels.classes.DTO;

import com.example.spring.tables.labels.classes.Label;
import com.example.spring.entityTemplates.DTO.NameIdDTOMapper;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class LabelDTOMapper implements Function<Label, LabelDTO> {

    @Override
    public LabelDTO apply(Label label) {
        return new LabelDTO(
                label.getId(),
                label.getName(),
                new PageImpl<>(label.getBands()
                        .stream()
                        .map(new NameIdDTOMapper()).collect(Collectors.toList()))
        );
    }
}

package com.example.spring.tables.labels.classes.DTO;

import com.example.spring.entityTemplates.DTO.NameIdDTO;
import com.example.spring.entityTemplates.DTO.NameIdDTOMapper;
import com.example.spring.entityTemplates.DTO.TableDTO;
import com.example.spring.tables.labels.classes.Label;
import com.example.spring.utils.OffsetLimitPageable;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.stream.Collectors;

public record LabelDTO(
        int id,
        String name,
        Page<NameIdDTO> bandPage
) implements TableDTO {
    public static LabelDTO DTOFactory(Label label, Pageable pageable) {
        int[] regulators = new int[3];
        regulators = OffsetLimitPageable.regulate(label,pageable,regulators);
        int size = regulators[0];
        int offset = regulators[1];
        int limit = regulators[2];

        return new LabelDTO(
                    label.getId(),
                    label.getName(),
                    new PageImpl<>(label.getBands().stream().skip(offset)
                            .map(new NameIdDTOMapper())
                            .collect(Collectors.toList())
                            .subList(0,limit), pageable, label.getBands().size())
            );
    }
}

package com.example.spring.tables.labels;

import com.example.spring.entityTemplates.extended.ExtendedBaseRepo;
import com.example.spring.tables.labels.classes.DTO.LabelDTO;
import com.example.spring.tables.labels.classes.Label;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LabelRepository extends ExtendedBaseRepo<Label, Integer, LabelDTO> {
    List<Label> findLabelsByNameContaining(String name, Pageable pageable);

}

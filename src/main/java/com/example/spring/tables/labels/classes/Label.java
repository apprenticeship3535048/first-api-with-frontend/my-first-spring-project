package com.example.spring.tables.labels.classes;

import com.example.spring.tables.bands.classes.Band;
import com.example.spring.tables.releases.classes.Release;
import com.example.spring.entityTemplates.extended.ExtendedBaseEntity;
import jakarta.persistence.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.*;

@Entity
@Table(name = "labels")
@SQLDelete(sql = "UPDATE labels SET state = 'deleted', deleted_at = current_date where id=?")
@Where(clause = "state = 'published'")
public class Label extends ExtendedBaseEntity {

    @OneToMany(mappedBy = "label")
    private List<Release> releases = new ArrayList<>();
    //label is the parent of releases

    @OneToMany(mappedBy = "label")
    private List<Band> bands = new ArrayList<>();
    //label is the parent of bands

    public Label() {
        super();
    }
    //still magical


    //getters and setters
    public List<Band> getBands() {
        return bands;
    }

    public void setBands(List<Band> bands) {
        this.bands = bands;
    }

    public List<Release> getReleases() {
        return releases;
    }

    public void setReleases(List<Release> releases) {
        this.releases = releases;
    }
}

package com.example.spring.tables.labels.classes;

import com.example.spring.entityTemplates.extended.ExtendedBaseService;
import com.example.spring.tables.labels.LabelRepository;
import com.example.spring.tables.labels.classes.DTO.LabelDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LabelService extends ExtendedBaseService {
    @Autowired
    LabelRepository labelRepository;

    protected LabelService(LabelRepository labelRepository, LabelDTOMapper labelDTOMapper) {
        super(labelRepository, labelDTOMapper);
    }

    //add a new Label to the DB
    public Label createNew(Label label) {
        return labelRepository.save(label);
    }

    //update a Label
    public Label update(int id, Label update) {
        Label label = (Label) getById(id);
        if (update.getName() != null) label.setName(update.getName());
        return labelRepository.save(label);
    }

}

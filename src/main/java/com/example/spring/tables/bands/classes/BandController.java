package com.example.spring.tables.bands.classes;


import com.example.spring.entityTemplates.extended.ExtendedBaseController;
import com.example.spring.tables.bands.classes.DTO.BandDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/bands")
public class BandController extends ExtendedBaseController {
    /*@Autowired
    BandRepository bandsRepository;*/
    //@Autowired
    BandService bandService;
    /*@Autowired
    LabelService labelService;*/

    @Autowired
    public BandController(BandService service, BandDTOMapper bandDTOMapper) {
        super(service, bandDTOMapper);
        this.bandService = service;
    }

    /*@ResponseStatus(HttpStatus.OK)
    @GetMapping("/name/{id}")
    public String getBandNameById(@PathVariable int id) {
        return bandService.getBandNameByID(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/yearFounded/{id}")
    public int getYearFoundedById(@PathVariable int id) {
        return bandService.getBandYearFoundedByID(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}/label")
    public Label getBandLabel(@PathVariable int id) {
        return bandService.getBandLabelByID(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{id}/label/DTO")
    public LabelDTO getBandLabelDTO(@PathVariable int id) {
        return bandService.getBandLabelDTOByID(id);
    }
    //Maybe not really needed

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}/label/name")
    public String getBandLabelName(@PathVariable int id) {
        return bandService.getBandLabelNameByID(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}/members")
    public Set<Member> getBandMembers(@PathVariable int id) {
        return bandService.getMembersByBandID(id);
    }
*/
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    public Band createNew(@RequestBody Band band) {

        return bandService.createNew(band);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public Band updateEntry(@PathVariable int id, @RequestBody Band band) {

        return bandService.update(id, band);
    }

   /* @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}/members/DTO")
    public List<MemberDTO> getBandMembersDTO(@PathVariable int id) {
        return bandService.getMemberDTOsByBandID(id);
    }*/

    /*@ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}/name")
    public Band updateName(@PathVariable int id, @RequestBody Band bands) {
        Band band = (Band) service.getById(id);
        band.setName(bands.getName());
        return (Band) bandsRepository.save(band);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}/year")
    public Band updateYear(@PathVariable int id, @RequestBody Band bands) {
        Band band = (Band) service.getById(id);
        band.setYearFounded(bands.getYearFounded());
        return (Band) bandsRepository.save(band);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}/label")
    public Band updateLabel(@PathVariable int id, @RequestBody Band bands) {
        Band band = (Band) service.getById(id);
        band.setLabel(bands.getLabel());
        return (Band) bandsRepository.save(band);
    }*/

    /*@GetMapping("/search")
    public List<Band> getByName(@RequestParam String name, @RequestParam int limit, @RequestParam int offset) {
        return bandService.searchByName(name, limit, offset);
    }

    @GetMapping("/mapped-search")
    public List<BandDTO> getByNameMapped(@RequestParam String name, @RequestParam int limit, @RequestParam int offset) {
        return (List<BandDTO>) bandService.searchByName(name, limit, offset)
                .stream().
                map(mapper)
                .collect(Collectors.toList());
    }*/


 /*@GetMapping("/test/")
    public void exe(HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.sendRedirect("/3/");
    }*/
    //in case of necessary redirects this might be helpful

}

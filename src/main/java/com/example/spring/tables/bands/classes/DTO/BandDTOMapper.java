package com.example.spring.tables.bands.classes.DTO;

import com.example.spring.tables.bands.classes.Band;
import com.example.spring.tables.bandsMembersRoles.classes.BandsMembersRoles;
import com.example.spring.tables.bandsMembersRoles.classes.DTO.MemberRolesDTOMapper;
import com.example.spring.tables.bandsMembersRoles.classes.MemberRoles;
import com.example.spring.tables.labels.classes.Label;
import com.example.spring.tables.members.classes.Member;
import com.example.spring.tables.releases.classes.DTO.ReleaseDTOMapper;
import com.example.spring.tables.roles.classes.Role;
import com.example.spring.entityTemplates.DTO.NameIdDTO;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class BandDTOMapper implements Function<Band, BandDTO> {

    @Override
    public BandDTO apply(Band band) {
        return new BandDTO(
                band.getId(),
                band.getName(),
                band.getYearFounded(),
                getLabelNameId(band),
                new PageImpl<>(band.getReleases()
                        .stream()
                        .map(new ReleaseDTOMapper())
                        .collect(Collectors.toList())),
                sortMembersRoles(band)
                        .stream()
                        .map(new MemberRolesDTOMapper())
                        .collect(Collectors.toList())
        );
    }

    public List<MemberRoles> sortMembersRoles(Band band) {
        List<Member> members = new ArrayList<>();
        List<ArrayList<Role>> roles = new ArrayList<>();
        List<MemberRoles> memberRoles = new ArrayList<>();

        for (BandsMembersRoles bmd : band.getMembersRoles()) {
            if (!members.contains(bmd.getMember())) {
                members.add(bmd.getMember());
            }
        }

        for (int i = 0; i < members.size(); i++) {
            for (BandsMembersRoles bmd : band.getMembersRoles()) {

                roles.add(new ArrayList<>());
                ArrayList<Role> rolesList = roles.get(i);
                if (bmd.getMember() == members.get(i) && !rolesList.contains(bmd.getRole())) {
                    rolesList.add(bmd.getRole());
                }
            }
            MemberRoles memberRoles1 = new MemberRoles(members.get(i), roles.get(i));
            memberRoles.add(memberRoles1);
        }
        return memberRoles;
    }

    public NameIdDTO getLabelNameId(Band band) {
        Label label = band.getLabel();
        if (label == null) {
            return null;
        }
        return new NameIdDTO(label.getId(), label.getName());
    }
}

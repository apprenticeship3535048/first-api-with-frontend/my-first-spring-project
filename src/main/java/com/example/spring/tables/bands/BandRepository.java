package com.example.spring.tables.bands;

import com.example.spring.entityTemplates.extended.ExtendedBaseRepo;
import com.example.spring.tables.bands.classes.Band;
import com.example.spring.tables.bands.classes.DTO.BandDTO;
import org.springframework.stereotype.Repository;

@Repository
public interface BandRepository extends ExtendedBaseRepo<Band, Integer, BandDTO> {
    //List<Band> findBandsByNameContaining(String name, Pageable pageable);
}

package com.example.spring.tables.bands.classes;

import com.example.spring.entityTemplates.extended.ExtendedBaseService;
import com.example.spring.tables.bands.BandRepository;
import com.example.spring.tables.bands.classes.DTO.BandDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BandService extends ExtendedBaseService {


    BandRepository bandsRepository;

    /*@Autowired
    LabelService labelService;*/


    BandDTOMapper mapper;

    /*public BandService(BandRepository bandsRepository, BandDTOMapper bandDTOMapper) {
        super(bandsRepository, bandDTOMapper);
        this.mapper = bandDTOMapper;
    }*/
    @Autowired
    public BandService(BandRepository repo, BandDTOMapper function) {
        super(repo, function);
        this.bandsRepository = repo;
        this.mapper = function;
    }

    /*public String getBandNameByID(int id) {
        Band band = bandsRepository.findById(id).orElseThrow(() -> {
            throw new IdNotFoundException(getTableClass(), id);
        });
        return band.getName();
    }

    public int getBandYearFoundedByID(int id) {
        Band band = bandsRepository.findById(id).orElseThrow(() -> {
            throw new IdNotFoundException(getTableClass(), id);
        });
        return band.getYearFounded();
    }

    //getting Parent-info
    public String getBandLabelNameByID(int id) {
        Band band = bandsRepository.findById(id).orElseThrow(() -> {
            throw new IdNotFoundException(getTableClass(), id);
        });
        return band.getLabel().getName();
    }

    public Label getBandLabelByID(int id) {
        Band band = bandsRepository.findById(id).orElseThrow(() -> {
            throw new IdNotFoundException(getTableClass(), id);
        });
        return band.getLabel();
    }

    public LabelDTO getBandLabelDTOByID(int id) {
        Label label = getBandLabelByID(id);
        if (label == null) {
            return null;
        }
        return new LabelDTO(label.getId(), label.getName(),
                label.getBands().stream().map(new NameIdDTOMapper()).collect(Collectors.toList()));
    }
    //get all members in a specific band
    public Set<Member> getMembersByBandID(int id) {
        //using Hashset to avoid duplicates
        Band band = bandsRepository.findById(id)
                .orElseThrow(() -> {
                    throw new IdNotFoundException(getTableClass(), id);
                });
        List<BandsMembersRoles> list = band.getMembersRoles();
        Set<Member> members = new HashSet<>();

        for (BandsMembersRoles bmr : list) {
            members.add(bmr.getMember());
        }
        return members;
    }

    public List<MemberDTO> getMemberDTOsByBandID(int id) {
        Set<Member> memberSet = getMembersByBandID(id);
        if (memberSet == null) {
            return null;
        }
        return memberSet.stream().map(new MemberDTOMapper()).collect(Collectors.toList());
    }
*/

    public Band createNew(Band band) {

        return bandsRepository.save(band);
    } //add a new Band to the DB


    public Band update(int id, Band update) {
        Band band = (Band) getById(id);
        if (update.getName() != null) band.setName(update.getName());
        if (update.getLabel() != null) band.setLabel(update.getLabel());
        if (update.getYearFounded() != null) band.setYearFounded(update.getYearFounded());
        if (update.getReleases() != null) band.setReleases(update.getReleases());
        return bandsRepository.save(band);
    } //update a Band

    /*public List<Band> searchByName (String name, int limit, int offset){
        Pageable pageable = new OffsetLimitPageable(limit, offset);
        List<Band> bands = bandsRepository.findBandsByNameContaining(name,pageable);
        return bands;
    }*/


}

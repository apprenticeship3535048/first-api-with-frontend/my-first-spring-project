package com.example.spring.tables.bands.classes;

import com.example.spring.tables.bandsMembersRoles.classes.BandsMembersRoles;
import com.example.spring.tables.bandsMembersRoles.classes.MemberRoles;
import com.example.spring.tables.labels.classes.Label;
import com.example.spring.tables.members.classes.Member;
import com.example.spring.tables.releases.classes.Release;
import com.example.spring.tables.roles.classes.Role;
import com.example.spring.entityTemplates.extended.ExtendedBaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.*;

@Entity
@Table(name = "bands")
@SQLDelete(sql = "UPDATE bands SET state = 'deleted', deleted_at = current_date where id=?")
@Where(clause = "state = 'published'")
public class Band extends ExtendedBaseEntity {

    @Column(name = "year_founded")
    private Integer yearFounded;


    // bands is the parent of releases
    @ManyToMany
    @JoinTable(name = "bands_releases",
            joinColumns = @JoinColumn(name = "band_id"),
            inverseJoinColumns = @JoinColumn(name = "release_id"))
    private List<Release> releases = new ArrayList<>();


    //in the output Json object band is viewed as the parent of members and roles
    @OneToMany(mappedBy = "band")
    private List<BandsMembersRoles> members_roles;

    @JsonBackReference(value = "band_label")
    @JoinColumn(name = "label_id")
    //tells the JPA when building the object that band is a child of label
    @ManyToOne(fetch = FetchType.LAZY)
    private Label label;

    //Needed for magical-springboot reasons
    public Band() {
        super();
    }

    public List<BandsMembersRoles> getMembersRoles() {
        return members_roles;
    }

    public void setMembersRoles(List<BandsMembersRoles> members_roles) {
        this.members_roles = members_roles;
    }

    public Integer getYearFounded() {
        return yearFounded;
    }

    public void setYearFounded(Integer yearFounded) {
        this.yearFounded = yearFounded;
    }

    public List<Release> getReleases() {
        return releases;
    }

    public void setReleases(List<Release> releases) {
        this.releases = releases;
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "Band{" +
                ", yearFounded=" + yearFounded +
                ", releases=" + releases +
                ", members_roles=" + members_roles +
                ", label=" + label +
                "}";
    }

    public List<MemberRoles> sortMembersRoles() {
        List<Member> members = new ArrayList<>();
        List<ArrayList<Role>> roles = new ArrayList<>();
        List<MemberRoles> memberRoles = new ArrayList<>();

        int i = 0;
        for (BandsMembersRoles bmd : getMembersRoles()) {
            ArrayList<Role> rolesList = new ArrayList<>();
            if (!members.contains(bmd.getMember())) {
                members.add(bmd.getMember());
                roles.add(new ArrayList<>());
                rolesList = roles.get(i++);
            } else {
                rolesList = roles.get(members.indexOf(bmd.getMember()));
                rolesList.add(bmd.getRole());
            }
        }

        for (int x = 0; x < members.size(); x++) {
            MemberRoles memberRoles1 = new MemberRoles(members.get(i), roles.get(i));
            memberRoles.add(memberRoles1);
        }
        return memberRoles;
    }
}
package com.example.spring.tables.members.classes.DTO;

import com.example.spring.tables.bandsMembersRoles.classes.BandsMembersRoles;
import com.example.spring.tables.members.classes.Member;
import com.example.spring.tables.roles.classes.Role;
import com.example.spring.entityTemplates.DTO.NameIdDTO;
import com.example.spring.entityTemplates.DTO.NameIdDTOMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class MemberDTOMapper implements Function<Member, MemberDTO> {

    @Override
    public MemberDTO apply(Member member) {
        return new MemberDTO(
                member.getId(),
                member.getName(),
                getRoles(member)
        );
    }

    private List<NameIdDTO> getRoles(Member member) {
        List<BandsMembersRoles> bmrList = member.getBandsMembersRolesList();
        List<Role> roles = new ArrayList<>();
        for (BandsMembersRoles bmr : bmrList) {
            roles.add(bmr.getRole());
        }
        return roles.stream().map(new NameIdDTOMapper()).collect(Collectors.toList());
    }
}

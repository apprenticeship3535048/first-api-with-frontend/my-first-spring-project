package com.example.spring.tables.members.classes;

import com.example.spring.entityTemplates.extended.ExtendedBaseController;
import com.example.spring.tables.members.MemberRepository;
import com.example.spring.tables.members.classes.DTO.MemberDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/members")
public class MemberController extends ExtendedBaseController {
    @Autowired
    MemberRepository memberRepository;
    @Autowired
    MemberService memberService;

    protected MemberController(MemberService memberService, MemberDTOMapper memberDTOMapper) {
        super(memberService, memberDTOMapper);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    public Member createNew(@RequestBody Member member) {

        return memberService.createNew(member);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public Member updateEntry(@PathVariable int id, @RequestBody Member member) {
        return memberService.update(id, member);
    }

}

package com.example.spring.tables.members;

import com.example.spring.entityTemplates.extended.ExtendedBaseRepo;
import com.example.spring.tables.members.classes.DTO.MemberDTO;
import com.example.spring.tables.members.classes.Member;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends ExtendedBaseRepo<Member, Integer, MemberDTO> {
}

package com.example.spring.tables.members.classes.DTO;

import com.example.spring.entityTemplates.DTO.NameIdDTO;
import com.example.spring.entityTemplates.DTO.TableDTO;

import java.util.List;

public record MemberDTO(
        int id,
        String name,
        List<NameIdDTO> roles
) implements TableDTO {
}

package com.example.spring.tables.members.classes;

import com.example.spring.tables.bandsMembersRoles.classes.BandsMembersRoles;
import com.example.spring.entityTemplates.extended.ExtendedBaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.List;

@Entity
@Table(name = "members")
@SQLDelete(sql = "UPDATE members SET state = 'deleted', deleted_at = current_date where id=?")
@Where(clause = "state = 'published'")
public class Member extends ExtendedBaseEntity {

    @JsonBackReference(value = "members")
    @OneToMany(mappedBy = "member")
    private List<BandsMembersRoles> bandsMembersRolesList;
    //member is a child of bands

    public Member() {
        super();
    }
    //You're a wizard Harry


    //getters and setters
    public List<BandsMembersRoles> getBandsMembersRolesList() {
        return bandsMembersRolesList;
    }

    public void setBandsMembersRolesList(List<BandsMembersRoles> bandsMembersRolesList) {
        this.bandsMembersRolesList = bandsMembersRolesList;
    }
}

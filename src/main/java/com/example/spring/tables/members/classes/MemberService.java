package com.example.spring.tables.members.classes;

import com.example.spring.entityTemplates.extended.ExtendedBaseService;
import com.example.spring.tables.members.MemberRepository;
import com.example.spring.tables.members.classes.DTO.MemberDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberService extends ExtendedBaseService {
    @Autowired
    MemberRepository memberRepository;

    protected MemberService(MemberRepository memberRepository, MemberDTOMapper memberDTOMapper) {
        super(memberRepository, memberDTOMapper);
    }

    //add a new Member to the DB
    public Member createNew(Member member) {

        return memberRepository.save(member);
    }

    //update a Member
    public Member update(int id, Member update) {
        Member member = (Member) getById(id);
        if (update.getName() != null) member.setName(update.getName());
        return memberRepository.save(member);
    }
}

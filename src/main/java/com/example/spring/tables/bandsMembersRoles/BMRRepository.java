package com.example.spring.tables.bandsMembersRoles;

import com.example.spring.entityTemplates.base.BaseEntity;
import com.example.spring.entityTemplates.base.BaseRepo;
import com.example.spring.entityTemplates.DTO.TableDTO;

public interface BMRRepository extends BaseRepo<BaseEntity, Integer, TableDTO> {
}

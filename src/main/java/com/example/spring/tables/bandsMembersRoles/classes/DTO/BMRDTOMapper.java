package com.example.spring.tables.bandsMembersRoles.classes.DTO;

import com.example.spring.tables.bandsMembersRoles.classes.BandsMembersRoles;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class BMRDTOMapper implements Function<BandsMembersRoles, BMRDTO> {

    @Override
    public BMRDTO apply(BandsMembersRoles bmr) {
        return new BMRDTO(
                bmr.getId(),
                bmr.getBand().getId(),
                bmr.getMember().getId(),
                bmr.getRole().getId()
        );
    }
}

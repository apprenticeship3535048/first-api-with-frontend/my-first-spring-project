package com.example.spring.tables.bandsMembersRoles.classes;

import com.example.spring.tables.bands.classes.Band;
import com.example.spring.tables.members.classes.Member;
import com.example.spring.tables.roles.classes.Role;
import com.example.spring.entityTemplates.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import org.hibernate.annotations.SQLDelete;

@Entity
@Table(name = "bands_members_roles")
@SQLDelete(sql = "UPDATE bands_members_roles SET state = 'deleted' where id=?")
public class BandsMembersRoles extends BaseEntity {

    //tells the id what is to be shown. -> @JsonBackReference means it won't be
    @JsonBackReference(value = "bands")
    //using Many to one here instead of many-to-many because it's an in-between table
    @ManyToOne
    @JoinColumn(name = "band_id", referencedColumnName = "id")
    //references band
    private Band band;

    @ManyToOne
    @JoinColumn(name = "member_id", referencedColumnName = "id")
    //references members
    private Member member;

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    //references roles
    private Role role;

    public BandsMembersRoles(Band band, Member member, Role role) {
        this.band = band;
        this.member = member;
        this.role = role;
    }

    public BandsMembersRoles() {
    }

    public Band getBand() {
        return band;
    }

    public void setBand(Band band) {
        this.band = band;
    }

    //will be shown
    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    //will be shown
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}

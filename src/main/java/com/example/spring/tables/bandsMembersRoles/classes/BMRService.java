package com.example.spring.tables.bandsMembersRoles.classes;

import com.example.spring.entityTemplates.base.BaseEntity;
import com.example.spring.tables.bands.classes.Band;
import com.example.spring.tables.bandsMembersRoles.BMRRepository;
import com.example.spring.tables.bandsMembersRoles.classes.DTO.BMRDTOMapper;
import com.example.spring.tables.members.classes.Member;
import com.example.spring.tables.roles.classes.Role;
import com.example.spring.entityTemplates.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class BMRService extends BaseService {

    @Autowired
    BMRRepository bmrRepository;

    private final BMRDTOMapper bmrDTOMapper;

    protected BMRService(BMRRepository repository, BMRDTOMapper function, BMRDTOMapper bmrDTOMapper) {
        super(repository,function);
        this.bmrDTOMapper = bmrDTOMapper;
    }

    public Set<Member> getMembersByBandId(int bandId) {
        Set<Member> members = new HashSet<>();
        for(BaseEntity entity : bmrRepository.findAll()) {
            BandsMembersRoles bmr = (BandsMembersRoles) entity;
            if(bmr.getBand().getId() == bandId) {
                members.add(bmr.getMember());
            }
        }
        return members;
    }

    public Set<Role> getRolesByMemberId(int memberId) {
        Set<Role> roles = new HashSet<>();
        for(BaseEntity entity : bmrRepository.findAll()) {
            BandsMembersRoles bmr = (BandsMembersRoles) entity;
            if(bmr.getMember().getId() == memberId) {
                roles.add(bmr.getRole());
            }
        }
        return roles;
    }

    public Set<Band> getBandsByMemberId(int memberId) {
        Set<Band> bands = new HashSet<>();
        for(BaseEntity entity : bmrRepository.findAll()) {
            BandsMembersRoles bmr = (BandsMembersRoles) entity;
            if(bmr.getMember().getId() == memberId) {
                bands.add(bmr.getBand());
            }
        }
        return bands;
    }

    public BandsMembersRoles createNew(BandsMembersRoles bmr) {

        return bmrRepository.save(bmr);
    } //add a new Band to the DB

    public BandsMembersRoles update(int id, BandsMembersRoles update){
        BandsMembersRoles bmr = (BandsMembersRoles) bmrRepository.getById(id);
        if (update.getBand() != null) bmr.setBand(update.getBand());
        if (update.getMember() != null) bmr.setMember(update.getMember());
        if (update.getRole() != null) bmr.setRole(update.getRole());
        return bmrRepository.save(bmr);
    }
}

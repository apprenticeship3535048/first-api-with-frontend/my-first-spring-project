package com.example.spring.tables.bandsMembersRoles.classes;

import com.example.spring.tables.bands.BandRepository;
import com.example.spring.tables.bandsMembersRoles.classes.DTO.BMRDTOMapper;
import com.example.spring.tables.labels.classes.LabelService;
import com.example.spring.entityTemplates.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/bmr")
public class BMRController extends BaseController {

    @Autowired
    BandRepository bandsRepository;
    @Autowired
    BMRService bmrService;
    @Autowired
    LabelService labelService;

    public BMRController(BMRService service, BMRDTOMapper bmrDTOMapper) {
        super(service, bmrDTOMapper);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    public BandsMembersRoles createNew(@RequestBody BandsMembersRoles bmr) {

        return bmrService.createNew(bmr);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public BandsMembersRoles updateEntry(@PathVariable int id, @RequestBody BandsMembersRoles bmr) {

        return bmrService.update(id, bmr);
    }
}

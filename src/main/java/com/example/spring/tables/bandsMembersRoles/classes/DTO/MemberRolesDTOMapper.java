package com.example.spring.tables.bandsMembersRoles.classes.DTO;

import com.example.spring.tables.bandsMembersRoles.classes.MemberRoles;
import com.example.spring.tables.members.classes.DTO.MemberDTO;
import com.example.spring.entityTemplates.DTO.NameIdDTOMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class MemberRolesDTOMapper implements Function<MemberRoles, MemberDTO> {

    @Override
    public MemberDTO apply(MemberRoles memberRoles) {
        return new MemberDTO(
                memberRoles.getMember().getId(),
                memberRoles.getMember().getName(),
                memberRoles.getRoles().stream().map(new NameIdDTOMapper()).collect(Collectors.toList())
        );
    }
}

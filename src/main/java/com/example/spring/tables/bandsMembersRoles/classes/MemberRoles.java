package com.example.spring.tables.bandsMembersRoles.classes;

import com.example.spring.tables.members.classes.Member;
import com.example.spring.tables.roles.classes.Role;

import java.util.List;

public class MemberRoles {

    Member member;

    List<Role> roles;

    public MemberRoles(Member member, List<Role> roles) {
        this.member = member;
        this.roles = roles;
    }

    public MemberRoles() {
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}

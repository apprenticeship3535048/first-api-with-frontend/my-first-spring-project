package com.example.spring.tables.bandsMembersRoles.classes.DTO;

import com.example.spring.entityTemplates.DTO.TableDTO;

public record BMRDTO(
        int id,
        int band_id,
        int member_id,
        int role_id
) implements TableDTO {
}

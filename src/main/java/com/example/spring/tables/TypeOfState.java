package com.example.spring.tables;

public enum TypeOfState {
    published,
    deleted
}

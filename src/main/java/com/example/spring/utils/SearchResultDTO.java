package com.example.spring.utils;

import com.example.spring.entityTemplates.DTO.TableDTO;

import java.util.List;

public class SearchResultDTO<Entity extends TableDTO> {

    int amountOfResults;

    List<Entity> results;

    public SearchResultDTO(int amountOfResults, List<Entity> results) {
        this.amountOfResults = amountOfResults;
        this.results = results;
    }

    public int getAmountOfResults() {
        return amountOfResults;
    }

    public void setAmountOfResults(int amountOfResults) {
        this.amountOfResults = amountOfResults;
    }

    public List<Entity> getResults() {
        return results;
    }

    public void setResults(List<Entity> results) {
        this.results = results;
    }
}

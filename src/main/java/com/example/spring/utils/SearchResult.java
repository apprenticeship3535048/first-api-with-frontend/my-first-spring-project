package com.example.spring.utils;

import com.example.spring.entityTemplates.extended.ExtendedBaseEntity;

import java.util.List;

public class SearchResult<Entity extends ExtendedBaseEntity> {

    int amountOfResults;

    List<Entity> results;

    public SearchResult(int amountOfResults, List<Entity> results) {
        this.amountOfResults = amountOfResults;
        this.results = results;
    }

    public int getAmountOfResults() {
        return amountOfResults;
    }

    public void setAmountOfResults(int amountOfResults) {
        this.amountOfResults = amountOfResults;
    }

    public List<Entity> getResults() {
        return results;
    }

    public void setResults(List<Entity> results) {
        this.results = results;
    }
}

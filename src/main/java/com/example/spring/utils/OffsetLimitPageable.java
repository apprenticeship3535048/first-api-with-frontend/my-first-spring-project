package com.example.spring.utils;

import com.example.spring.entityTemplates.base.BaseEntity;
import com.example.spring.entityTemplates.extended.ExtendedBaseEntity;
import com.example.spring.tables.bands.classes.Band;
import com.example.spring.tables.labels.classes.Label;
import com.example.spring.tables.releases.classes.Release;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class OffsetLimitPageable implements Pageable {

    private int limit;
    private int offset;
    private int page;
    private Sort sort = Sort.unsorted();

    public OffsetLimitPageable(int limit, int page) {
        this.limit = limit;
        this.offset = limit*(page-1);
        this.page = page;
    }

    @Override
    public int getPageNumber() {
        return page;
    }

    @Override
    public int getPageSize() {
        return limit;
    }

    @Override
    public long getOffset() {
        if (offset < 0) offset = offset * -1;
        return offset;
    }

    @Override
    public Sort getSort() {
        return sort;
    }

    @Override
    public Pageable next() {
        return null;
    }

    @Override
    public Pageable previousOrFirst() {
        return null;
    }

    @Override
    public Pageable first() {
        return null;
    }

    @Override
    public Pageable withPage(int pageNumber) {
        return null;
    }

    @Override
    public boolean hasPrevious() {
        if (page == 0) return false;
        else return true;
    }

    public static int[] regulate(BaseEntity entity, Pageable pageable, int[] regulators){
        int size = 0,offset = 0,limit = 0;

        if (entity instanceof Label){
            Label label = (Label) entity;
            size = label.getBands().size();
        } else if (entity instanceof Band) {
            Band band = (Band) entity;
            size = band.getReleases().size();
        } else if (entity instanceof Release) {
            Release release = (Release) entity;
            size = release.getSongs().size();
        }
        offset = (int) pageable.getOffset();
        limit = Math.min(pageable.getPageSize(), size);

        if (size == 0 || limit == 0) {
            offset = 0;
            limit = 0;
        } /*if the size of the label.getBands() array is zero, limit and offset will be set to zero too*/
        else if (size == 1){
            limit = 1;
            offset = 0;
        } /*if the size of the label.getBands() array is zero, limit will be set to one and offset will be set to zero */
        else if (size < offset || size - offset < limit) {
            offset = size-1;
            limit = 1;
        } /*if the size of the array is smaller than the offset or the remaining size is smaller than the offset
             the last entry will be shown*/
        regulators[0] = size;
        regulators[1] = offset;
        regulators[2] = limit;
        return regulators;
    }
}

<h1> Welcome to our wonderful API</h1>
This API concerns itself with filling a pre-made SQL-Database with tables that are tailored
to be used for a music database. After setting it up you can use it to fill said database with
Entries that match the description of our pre-defined entities.
<p>This is a guide meant to help you set everything up and get going ASAP.</p>

<h2>Step 1</h2>
You will have to set up a Database called "music" in MySQL and connect it
to this project via the Database-Tab found in the top right.

<h2>Step 2</h2>
You will have to navigate to src/resources within this project and open
the application.properties file. As the filename already suggests
you can find the general settings for this project in here.
You will have to change these settings to match your SQL Username and Password:

<p>spring.datasource.username=YOUR_USERNAME</p>
<p>spring.datasource.password=YOUR_PASSWORD</p>

<h2>Step 3</h2>

Hurray! Now you are ready to start the project!
<p>Now all you have to do is to execute the main class.</p>
You can do this by either clicking on the Drop-Down menu in the top right
(left of the play button) and selecting "Spring" or by 
navigating to src/java/Spring.java within the project and right-clicking
the Spring class to then execute it via the selection menu.

<h2>Step 4 (optional)</h2>
Last but not least you can find the file postMockData.sql int the SQL folder within this Project.
execute this file to have a few entries in your Database to start and play around with.

<p>Enjoy!</p>
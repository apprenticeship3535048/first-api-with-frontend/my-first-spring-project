# Headless - Spring-Application
Create a headless cms via java and spring framework


## Getting started

### Step - 1
- [ ] Create an appointment with your body to figure out, which topics are left to start

### Step - 2
Watch this videos to understand, how is going on with spring and REST
- [ ] Understand the spring Roadmap (https://www.youtube.com/watch?v=cehTm_oSrqA)
- [ ] Understand the spring (https://www.youtube.com/watch?v=9SGDpanrc8U)

### Step - 3
- [ ] Create an appointment with your body to create rough concept
- [ ] Fork this repo and create your own issues (for each Entity, Service, Controller, Repository and REST-API-Endpoints, you should have an issue)
- [ ] All entites have the fields "state", "created_at", "updated_at" and "deleted_at"
1. state, that can be used for a workflow for entities e.g. 
   1. published (if the entity is created or updated)
   2. deleted (if the entity is marked as deleted) -> pls. don't remove an entity from the DB, that we can see the changes of the states
2. created_at (DateTime for each entity)
3. updated_at (DateTime for each update on an entity)
4. deleted_at (DateTime for each delete on an entity, if this field is filled, the entry is not to find in the DB-Queries)

### Step - 4
- [ ] Create your DB via SQL (without JPA / JAVA / Spring)

### Step - 5
- [ ] Create a structure for REST - API - Endpoints (CRUD for Backend)
- [ ] Create your Entities and connect with your DB-Tables (https://www.youtube.com/watch?v=8SGI_XS5OPw)
- [ ] Create your Services
- [ ] Create your Controllers
- [ ] Create your Repositories

### Step - 6
- [ ] Create Postman - Collections to CRUD the Entities directly via REST - API


## BONUS:
- [ ] Use Unit-Test to test your Application (https://youtu.be/Geq60OVyBPg or https://www.youtube.com/watch?v=z6gOPonp2t0)
- [ ] Security for REST - API (https://www.youtube.com/watch?v=TOox3CGarf8)


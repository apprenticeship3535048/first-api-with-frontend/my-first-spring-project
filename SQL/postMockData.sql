insert into labels(name)
    values ('Robotor Records'),
           ('Eagle Records'),
           ('Chrysalis'),
           ('EMI Records');



insert into bands (label_id, name, year_founded)
VALUES
    (3,'UFO',1968),
    (4,'Iron Maiden',1975),
    (1,'Pink Floyd', 1968),
    (1,'Elder', 1964),
    (null,'Kadavar',2010),
    (1,'Toto',1883),
    (1,'The Offspring',1883),
    (1,'Red Hot Chilli Peppers',1883),
    (1,'Papa Roach',1883),
    (1,'Babymetal',1883);

insert into bands (label_id, name, year_founded)
VALUES
    (3,'Fountains Of Wayne',1968),
    (3,'Arctic Monkeys',1975),
    (3,'Billy Idol', 1968),
    (3,'The Black Keys', 1964),
    (3,'Rise Against',2010),
    (2,'Three Days Grace',1883),
    (2,'Rage Against the Machine',1883),
    (2,'Kings Of Leon',1883),
    (2,'Bloodhound Gang',1883),
    (2,'The Killers',1883);


insert into releases (label_id, name, num_of_sales, type_of_release)
VALUES
    (1, 'Phenomenon',null,'Album'),
    (1, 'The Number Of The Beast',2582500, 'Album'),
    (1, 'The Wall',30000000,'Album'),
    (1, 'Eldovar',null,'Album'),
    (2, 'Goblins & Prostitutes',null,'Album'),
    (2, 'Propelled by Jealousy',2582500, 'Album'),
    (2, 'Wet otters are wet',30000000,'Album'),
    (2, 'Receipts and deceits',null,'Album'),
    (3, 'Water and other unhealthy things',null,'Album'),
    (3, 'Tattoos and Ponies',2582500, 'Album'),
    (3, 'Catcalling guitars',30000000,'Album'),
    (3, 'Mansplaining adulthood',null,'Album'),
    (4, 'Snails, Salt & The Afterlife',null,'Album'),
    (4, 'Eat, prey, Satan',2582500, 'Album'),
    (4, 'Dried mayonaise on plates',30000000,'Album'),
    (4, 'Wet teabags and other magical creatures',null,'Album'),
    (4, 'Sparkling joy of death and rainbows',null,'Album'),
    (4, 'Pearly whirlies',2582500, 'Album'),
    (4, 'Harvest moon',30000000,'Album'),
    (4, 'Popcorn Icecream',null,'Album'),
    (4, 'chopstick cake',null,'Album'),
    (4, 'horseburger',2582500, 'Album'),
    (4, 'ginger drinking a ginger ale',30000000,'Album'),
    (4, 'heavens to betsy',null,'Album');


insert into genres (name)
VALUES
    ('hard rock'),
    ('space rock'),
    ('heavy metal'),
    ('progressive pop'),
    ('progressive rock'),
    ('art rock'),
    ('alternative'),
    ('indie'),
    ('new age'),
    ('rock');



insert into members (name)
VALUES
-- band members ufo
    ('Michael Schenker'),
    ('Pete Way'),
    ('Phil Mogg'),
    ('Andy Parker'),
-- band members iron maiden
    ('Bruce Dickinson'),
    ('Dave Murray'),
    ('Adrian Smith'),
    ('Steve Harris'),
    ('Clive Burr'),
-- band members pink floyd
    ('Roger Waters'),
    ('David Gilmour'),
    ('Nick Mason'),
    ('Richard Wright'),
-- band members Elder
    ('Nick DiSalvo'),
    ('Mike Risberg'),
    ('Jack Donovan'),
    ('Georg Edert'),
-- band members Kadavar
    ('Christoph Lindermann'),
    ('Simon Bouteloup'),
    ('Christoph Bartelt');

insert into roles (name)
values ('electric guitar'),
       ('bass'),
       ('vocals'),
       ('drums'),
       ('synthesizer'),
       ('percussion'),
       ('keyboard'),
       ('hammond organ');

insert into songs (release_id, name, length)
VALUES
-- songs in phenomenon
    (1,'Oh My',146),
    (1,'Crystal Light',227),
    (1,'Doctor Doctor',250),
    (1,'Space Child',241),
    (1,'Rock Bottom',392),
    (1,'Too Young To Know',190),
    (1,'Time On My Hands',250),
    (1,'Built For Comfort',181),
    (1,'Lipstick Traces',140),
    (1,'Queen Of The Deep',349),

-- songs in the number of the beast
    (2,'Invaders',202),
    (2,'Children of the Damned',273),
    (2,'The Prisoner',360),
    (2,'22 Acacia Avenue',398),
    (2,'The Number of the Beast',291),
    (2,'Run to the Hills',230),
    (2,'Gangland',227),
    (2,'Total Eclipse',268),
    (2,'Hallowed be Thy Name',430),

-- songs in the wall
    (3,'In the Flesh?',196),
    (3,'The Thin Ice',147),
    (3,'Another Brick in the Wall, Part 1',106),
    (3,'The Happiest day of Our Lives',106),
    (3,'Another Brick in the Wall, Part 2',239),
    (3,'Mother',332),
    (3,'Empty Spaces',150),
    (3,'Young Lust',205),
    (3,'One of My Turns',221),
    (3,'Dont Leave Me Now',248),
    (3,'Another Brick in the Wall, Part 3',78),
    (3,'Goodbye Cruel World',76),
    (3,'Hey You',280),
    (3,'Is There Anybody Out There',164),
    (3,'Nobody Home',206),
    (3,'Vera',95),
    (3,'Bring the Boys Back Home',81),
    (3,'Comfortably Numb',383),
    (3,'The Show Must Go On',96),
    (3,'In the Flesh',255),
    (3,'Run Like Hell',260),
    (3,'Waiting for the Worms',244),
    (3,'Stop',30),
    (3,'The Trial',313),
    (3,'Outside the Wall',101),
    (3,'Goodbye Blue Sky',165),

-- songs in Eldavar
    (4,'From Deep Within',573),
    (4,'In the Way',338),
    (4,'El Matador',402),
    (4,'Rebirth of the Twins',179),
    (4,'Raspletin',299),
    (4,'Blood Moon Night',664),
    (4,'Cherry Trees',224);


insert into bands_releases(band_id, release_id)
VALUES
   (1,1),
   (2,2),
   (3,3),
   (4,4),
   (5,5),
   (1,6),
   (1,7),
   (1,8),
   (1,9),
   (2,10),
   (2,11),
   (2,12),
   (2,13),
   (3,14),
   (3,15),
   (3,16),
   (4,17),
   (4,18),
   (4,19),
   (4,20),
   (1,21),
   (1,22),
   (2,23),
   (2,24);

insert into bands_members_roles(band_id, member_id, role_id)
-- ufo band members and their roles
VALUES (1,1,1),
       (1,2,2),
       (1,3,3),
       (1,4,4),
-- iron maiden band members and their roles
       (2,5,3),
       (2,6,1),
       (2,7,1),
       (2,8,2),
       (2,9,4),
-- pink floyd band members and their roles
       (3,10,1),
       (3,10,2),
       (3,10,3),
       (3,10,5),
       (3,11,1),
       (3,11,2),
       (3,11,3),
       (3,11,5),
       (3,12,4),
       (3,12,6),
       (3,13,7),
       (3,13,8),
-- elder band members and their roles
       (4,14,1),
       (4,14,7),
       (4,14,3),
       (4,15,1),
       (4,16,2),
       (4,17,4),
-- kadavar band members and their roles
       (5,18,1),
       (5,18,3),
       (5,19,2),
       (5,20,4);


insert into releases_genres(release_id, genre_id)
VALUES
-- genres in album Phenomenon by UFO
    (1,1),
    (1,2),
-- genres in album The Number of the Beast by Iron Maiden
    (2,3),
-- genres in album the wall by pink floyd
    (3,4),
    (3,5),
    (3,6),
-- genres in album Eldovar
    (4,7),
    (4,8),
    (4,9),
    (4,10);

